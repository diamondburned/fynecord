package main

import (
	"fyne.io/fyne"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/widget"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/fynecord/guilds"
)

type channelView struct {
	scroll *widget.ScrollContainer
	box    *widget.Box

	children []*channelItem
}

type channelItem struct {
	*discordgo.Channel

	group   *widget.Group
	buttons []*widget.Button
}

func newChannelView() *channelView {
	c := &channelView{
		box: widget.NewVBox(),
	}

	c.scroll = widget.NewScrollContainer(c.box)
	return c
}

func (cv *channelView) setChannel(chs []*discordgo.Channel) {
	cats := guilds.Categorize(chs)

	cv.box.Children = make([]fyne.CanvasObject, len(cats))
	cv.children = make([]*channelItem, len(cats))

	for i, cat := range cats {
		g := widget.NewGroup(cat.Name)
		cv.children[i] = &channelItem{
			group:   g,
			buttons: make([]*widget.Button, len(cat.Children)),
		}

		for j, c := range cat.Children {
			b := widget.NewButton("   #"+c.Name, func() {
				println("Pressed: " + c.Name)
			})

			g.Append(b)
			cv.children[i].buttons[j] = b
		}

		cv.box.Children[i] = g
	}

	canvas.Refresh(cv.box)
}
