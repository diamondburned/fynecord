package main

import (
	"log"

	"fyne.io/fyne"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/widget"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/fynecord/widgets"
)

type guildView struct {
	scroll *widget.ScrollContainer
	box    *widget.Box

	children []*widgets.GuildButton
}

func newGuildView(gs []*discordgo.Guild) *guildView {
	gv := &guildView{}
	gv.children = make([]*widgets.GuildButton, len(gs))
	canvasobjs := make([]fyne.CanvasObject, len(gs))

	for i := range gv.children {
		gv.children[i] = widgets.NewGuildButton(gs[i])
		canvasobjs[i] = gv.children[i]
	}

	gv.box = widget.NewVBox(canvasobjs...)

	canvas.Refresh(gv.box)
	gv.scroll = widget.NewScrollContainer(gv.box)

	return gv
}

// downloads all guild icons, run this in goroutine
func (gv *guildView) updateGuildIcons() {
	for _, c := range gv.children {
		c.Refresh()
	}

	log.Println("Updated")
}

func (gv *guildView) loadGuild(i int) {}
