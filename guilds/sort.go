package guilds

import (
	"sort"

	"github.com/bwmarrin/discordgo"
)

// Category is a struct used for categorizing, or how Discord channels
// should've been implemented
type Category struct {
	*discordgo.Channel

	Children []*discordgo.Channel
}

// Categorize sorts channels into categories
func Categorize(cs []*discordgo.Channel) []*Category {
	p := map[string]*Category{}

	for _, c := range cs {
		if c.Type == discordgo.ChannelTypeGuildCategory {
			v, ok := p[c.ID]
			if ok {
				v.Channel = c
			} else {
				p[c.ID] = &Category{
					Channel: c,
				}
			}
		} else {
			v, ok := p[c.ParentID]
			if ok {
				v.Children = append(v.Children, c)
			} else {
				p[c.ParentID] = &Category{
					Children: []*discordgo.Channel{c},
				}
			}
		}
	}

	a := make([]*Category, 0, len(p))

	for _, v := range p {
		if v.Children != nil {
			sort.Slice(v.Children, func(i, j int) bool {
				return v.Children[i].Position < v.Children[j].Position
			})
		}

		a = append(a, v)
	}

	sort.Slice(a, func(i, j int) bool {
		return a[i].Position < a[j].Position
	})

	return a
}
