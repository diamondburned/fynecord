package guilds

import (
	"fmt"
	"testing"
)

func TestSort(t *testing.T) {
	cats := Categorize(testData.Channels)

	for _, cat := range cats {
		if cat.Name != "" {
			fmt.Println(cat.Name)
		} else {
			fmt.Println("No category")
		}

		for _, c := range cat.Children {
			fmt.Println("\t" + c.Name)
		}
	}
}
