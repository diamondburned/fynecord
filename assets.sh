#!/bin/sh
set -e

# This script generates the assets
fyne bundle -package assets -prefix "" assets/ > assets/assets.go

echo Done.
