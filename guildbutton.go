package main

import (
	"image/color"

	"fyne.io/fyne"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
)

// Literally the same buttonRenderer, only with Padding set to 0.
// If only this was exported...
type buttonRenderer struct {
	icon *canvas.Image

	objects []fyne.CanvasObject
	button  *guildButton
}

func (b *buttonRenderer) MinSize() fyne.Size {
	var min fyne.Size

	if b.icon != nil {
		min = fyne.NewSize(theme.IconInlineSize()+0*2, theme.IconInlineSize()+0*2)
	}

	return min
}

func (b *buttonRenderer) Layout(size fyne.Size) {
	b.icon.Resize(fyne.NewSize(theme.IconInlineSize(), theme.IconInlineSize()))
	b.icon.Move(fyne.NewPos(0, 0))
}

func (b *buttonRenderer) ApplyTheme() {
	b.Refresh()
}

func (b *buttonRenderer) BackgroundColor() color.Color {
	if b.button.Style == widget.PrimaryButton {
		return theme.PrimaryColor()
	}

	return theme.ButtonColor()
}

func (b *buttonRenderer) Refresh() {
	if b.button.Icon != nil {
		if b.icon == nil {
			b.icon = canvas.NewImageFromResource(b.button.Icon)
			b.objects = append(b.objects, b.icon)
		} else {
			b.icon.Resource = b.button.Icon
		}

		b.icon.Hidden = false
	} else if b.icon != nil {
		b.icon.Hidden = true
	}

	b.Layout(b.button.Size())
	canvas.Refresh(b.button)
}

func (b *buttonRenderer) Objects() []fyne.CanvasObject {
	return b.objects
}

func (b *buttonRenderer) Destroy() {
}

type guildButton struct {
	widget.Button
}

func (b *guildButton) CreateRenderer() fyne.WidgetRenderer {
	var icon *canvas.Image
	if b.Icon != nil {
		icon = canvas.NewImageFromResource(b.Icon)
	}

	text := canvas.NewText(b.Text, theme.TextColor())
	text.Alignment = fyne.TextAlignCenter

	objects := []fyne.CanvasObject{
		text,
	}

	if icon != nil {
		objects = append(objects, icon)
	}

	return &buttonRenderer{icon, objects, b}
}
