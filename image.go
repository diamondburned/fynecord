package main

import (
	"fmt"
	"image"
	"io/ioutil"
	"net/http"
	"sync"
	"time"

	// Shadow imports for supported image types
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
)

var httpClient = &http.Client{
	Timeout: 2 * time.Second,
}

var (
	imageCache = map[string]image.Image{}
)

// DownloadImage downloads the image. Supported formats are
// GIF, JPEG and PNG.
func DownloadImage(url string) (image.Image, error) {
	if i, ok := imageCache[url]; ok {
		return i, nil
	}

	r, err := httpClient.Get(url)
	if err != nil {
		return nil, err
	}

	defer r.Body.Close()

	img, _, err := image.Decode(r.Body)
	if err != nil {
		return nil, err
	}

	imageCache[url] = img

	return img, nil
}

var (
	rawCache      = map[string][]byte{}
	rawCacheMutex sync.Mutex
)

// DownloadRaw downloads raw body
func DownloadRaw(url string) ([]byte, error) {
	rawCacheMutex.Lock()

	if b, ok := rawCache[url]; ok {
		rawCacheMutex.Unlock()
		return b, nil
	}

	rawCacheMutex.Unlock()

	r, err := httpClient.Get(url)
	if err != nil {
		return nil, err
	}

	defer r.Body.Close()

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, fmt.Errorf("Invalid response code: %d\nContent: %s", r.StatusCode, string(b))
	}

	rawCacheMutex.Lock()
	defer rawCacheMutex.Unlock()

	rawCache[url] = b
	return b, nil
}
