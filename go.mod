module gitlab.com/diamondburned/fynecord

require (
	fyne.io/fyne v1.0.1
	github.com/bwmarrin/discordgo v0.19.0
	github.com/go-gl/gl v0.0.0-20190320180904-bf2b1f2f34d7 // indirect
	github.com/go-gl/glfw v0.0.0-20190409004039-e6da0acd62b1 // indirect
	github.com/josephspurrier/goversioninfo v0.0.0-20190209210621-63e6d1acd3dd // indirect
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/spf13/afero v1.2.2 // indirect
	github.com/srwiley/oksvg v0.0.0-20190414003808-c520f0a6c5cc // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	golang.org/x/crypto v0.0.0-20190422183909-d864b10871cd // indirect
	golang.org/x/image v0.0.0-20190424155947-59b11bec70c7 // indirect
	golang.org/x/net v0.0.0-20190424112056-4829fb13d2c6 // indirect
	golang.org/x/sys v0.0.0-20190424175732-18eb32c0e2f0 // indirect
	golang.org/x/text v0.3.1 // indirect
	golang.org/x/tools v0.0.0-20190424031103-cb2dda6eabdf // indirect
)
