package main

import (
	"log"
	"os"

	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/layout"
	"github.com/bwmarrin/discordgo"
)

func main() {
	token := os.Getenv("TOKEN")
	if token == "" {
		log.Fatal("Invalid token")
	}

	s, err := discordgo.New(token)
	if err != nil {
		panic(err)
	}

	ch := make(chan struct{})
	s.AddHandlerOnce(func(s *discordgo.Session, r *discordgo.Ready) {
		log.Println("Ready.")
		ch <- struct{}{}
	})

	if err := s.Open(); err != nil {
		panic(err)
	}

	defer s.Close()

	log.Println("Connected to Discord.")

	<-ch

	app := app.New()

	w := app.NewWindow("Hello")
	w.SetOnClosed(func() {
		s.Close()
	})

	gv := newGuildView(s.State.Guilds)
	gv.updateGuildIcons()

	g, err := s.State.Guild("361910177961738242")
	if err != nil {
		panic(err)
	}

	cv := newChannelView()
	cv.setChannel(g.Channels)

	log.Println("Created views.")

	w.SetContent(fyne.NewContainerWithLayout(
		layout.NewGridLayout(2),
		fyne.NewContainer(
			gv.scroll,
		), cv.scroll,
	))

	w.ShowAndRun()
}
