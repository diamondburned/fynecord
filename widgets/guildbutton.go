package widgets

import (
	"fmt"
	"image/color"
	"log"

	"fyne.io/fyne"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/fynecord/assets"
)

// Size is the dimension of each icon
const Size = 48

// GuildButton is the clickable button for one guild
type GuildButton struct {
	baseWidget

	icon  *canvas.Image
	guild *discordgo.Guild

	objects []fyne.CanvasObject

	OnTapped          func(*discordgo.Guild) `json:"-"`
	OnTappedSecondary func(*discordgo.Guild) `json:"-"`
}

// NewGuildButton makes a stateful guild button on the left bar
func NewGuildButton(g *discordgo.Guild) *GuildButton {
	gb := &GuildButton{
		guild: g,
		icon: canvas.NewImageFromResource(
			assets.Placeholder56Png,
		),
	}

	widget.Renderer(gb).Layout(gb.MinSize())
	return gb
}

// CreateRenderer returns itself
func (gb *GuildButton) CreateRenderer() fyne.WidgetRenderer {
	gb.objects = []fyne.CanvasObject{
		gb.icon,
	}

	return gb
}

// Size returns a static size equal to MinSize
func (gb *GuildButton) Size() fyne.Size {
	return fyne.Size{
		Width:  Size,
		Height: Size,
	}
}

// MinSize returns a static size
func (gb *GuildButton) MinSize() fyne.Size {
	return fyne.Size{
		Width:  Size,
		Height: Size,
	}
}

//Layout unsure what this does
func (gb *GuildButton) Layout(size fyne.Size) {
	gb.icon.Resize(size)
}

// ApplyTheme asd
func (*GuildButton) ApplyTheme() {}

// BackgroundColor asdufh
func (*GuildButton) BackgroundColor() color.Color {
	return theme.ButtonColor()
}

// Refresh asd
func (gb *GuildButton) Refresh() {
	log.Println("Refreshing for guild + " + gb.guild.Name)

	i, err := downloadImage(fmt.Sprintf(
		"https://cdn.discordapp.com/icons/%s/%s.png",
		gb.guild.ID, gb.guild.Icon,
	))

	if err != nil {
		gb.icon = canvas.NewImageFromResource(assets.Placeholder56Png)
	} else {
		gb.icon = canvas.NewImageFromImage(i)
		gb.objects = []fyne.CanvasObject{gb.icon}
		canvas.Refresh(gb.icon)
	}

	canvas.Refresh(gb)
}

// Objects returns a list of objs
func (gb *GuildButton) Objects() []fyne.CanvasObject {
	return gb.objects
}

// Destroy destroys the thing
func (gb *GuildButton) Destroy() {
	gb.icon = nil
}

// Visible returns object's visibility
func (gb *GuildButton) Visible() bool {
	return !gb.Hidden
}

// Show this widget, if it was previously hidden
func (gb *GuildButton) Show() {
	gb.show(gb)
}

// Hide this widget, if it was previously visible
func (gb *GuildButton) Hide() {
	gb.hide(gb)
}

// Tapped is called when a pointer tapped event is captured and triggers any tap handler
func (gb *GuildButton) Tapped(*fyne.PointEvent) {
	if gb.OnTapped != nil {
		gb.OnTapped(gb.guild)
	}
}

// TappedSecondary is called when a secondary pointer tapped event is captured
func (gb *GuildButton) TappedSecondary(*fyne.PointEvent) {
	if gb.OnTappedSecondary != nil {
		gb.OnTappedSecondary(gb.guild)
	}
}

// Resize sets a new size for a widget. This does nothing.
func (gb *GuildButton) Resize(size fyne.Size) {
	gb.icon.Resize(size)
}

// Move the widget to a new position, relative to it's parent.
// Note this should not be used if the widget is being managed by a Layout within a Container.
func (gb *GuildButton) Move(pos fyne.Position) {
	gb.move(pos, gb)
}
