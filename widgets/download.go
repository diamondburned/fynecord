package widgets

import (
	"image"
	"net/http"
	"time"
)

var httpClient = &http.Client{
	Timeout: 2 * time.Second,
}

var (
	imageCache = map[string]image.Image{}
)

// DownloadImage downloads the image. Supported formats are
// GIF, JPEG and PNG.
func downloadImage(url string) (image.Image, error) {
	if i, ok := imageCache[url]; ok {
		return i, nil
	}

	r, err := httpClient.Get(url)
	if err != nil {
		return nil, err
	}

	defer r.Body.Close()

	img, _, err := image.Decode(r.Body)
	if err != nil {
		return nil, err
	}

	imageCache[url] = img

	return img, nil
}
